﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ora1.Classes
{
    class temak
    {
        private string name;

        private string content;

        public temak(string content, string name)
        {
            Content = content;
            Name = name;
        }

        public string Content
        {
            get { return content; }
            set { content = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public override string ToString()
        {
            return this.name;
        }

    }
}
