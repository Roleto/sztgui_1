﻿using Newtonsoft.Json;
using ora1.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ora1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private temak currentTema;
        public MainWindow()
        {
            InitializeComponent();
            if(File.Exists("temak.json"))
            {
                var temaks = JsonConvert.DeserializeObject<temak[]>(File.ReadAllText("temak.json"));
                temaks.ToList().ForEach(x => lbox.Items.Add(x));
                lbox.SelectedIndex=0;
                currentTema = lbox.SelectedItem as temak;
                contentTb.Text = currentTema.Content;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(lbox.SelectedItem != null)
                (lbox.SelectedItem as temak).Content = contentTb.Text;
            temak t = new temak(string.Empty, textBox.Text);
            contentTb.Text = t.Content;
            this.currentTema = t;

            lbox.Items.Add(t);
            lbox.SelectedItem = this.currentTema;
            textBox.Text = string.Empty;
        }

        private void lbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (currentTema == null || lbox.SelectedItem == null)
                return;

            temak t = lbox.SelectedItem as temak;
            currentTema.Content = contentTb.Text;
            contentTb.Text = t.Content;
            currentTema = lbox.SelectedItem as temak;

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            (lbox.SelectedItem as temak).Content = contentTb.Text;
            List<temak> temaks = new List<temak>();
            foreach (temak item in lbox.Items)
            {
                temaks.Add(item);
            }
            string jsanData = JsonConvert.SerializeObject(temaks);
            File.WriteAllText("temak.json", jsanData);
        }

        private void lbox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lbox.Items.Remove(currentTema);
            lbox.SelectedIndex = 0;
            //ne törold ki mindent
            currentTema = lbox.SelectedItem as temak;
            contentTb.Text = currentTema.Content;
        }
    }
}
